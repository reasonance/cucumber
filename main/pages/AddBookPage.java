import org.openqa.selenium.sendKeys;
import org.openqa.selenium.click;

import main.helpers.Driver.Driver;
import main.helpers.TestData.Data;

public class AddBookAction 
{
	private TestData data;

	public AddBookAction()
	{}

	public void enter_title(WebElement title_area)
	{
		title_area.sendKeys(data.title);
	}

	public void enter_author(WebElement author_area)
	{
		author_area.sendKeys(data.author);
	}

	public void enter_tags(WebElement taghs_area)
	{
		taghs_area.sendKeys(data.tags);
	}

	public void enter_amount_in_location(WebElement location_area)
	{
		location_area.sendKeys(data.amount);
	}	

	public void request(WebElement create_button, WebElement request_button)
	{
		create_button.click();
		request_button.click();
	}
}