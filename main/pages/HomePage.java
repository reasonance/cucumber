import main.helpers.Driver.Driver;
import main.helpers.TestData.TestData;

public class HomePageAction 
{
	private TestData data; 

	public void go_to_profile(WebElement profile_button)
	{
		profile_button.click();
	}

	public void add_book(WebElement add_book_button)
	{
		add_book_button.click();
	}
}