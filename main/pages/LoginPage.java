import org.openqa.selenium.sendKeys;
import org.openqa.selenium.click;

import main.helpers.Driver.Driver;
import main.helpers.TestData;

public class SignInAction 
{
	private TestData data;

	public SignInAction()
	{
		Driver driver;
		driver.navigate().gotourl(data.link);
	}

	public void enter_email(WebElement email_area)
	{
		email_area.sendKeys(data.email);
	}

	public void enter_password(WebElement password_area)
	{
		password_area.sendKeys(data.password);
	}

	public void signIn(WebElement signInButton)
	{
		signInButton.click();
	}
}