
public class TestData
{
	public String link = "http://inlib-staging.dev:3000/";
	public String email = "vladyslavk@softwareplanet.uk.com";
	public String password = "password";

	public String title = "Authomated Testing Proffesional";
	public String author = "Vlasyslav Kovalenko";
	public String tags = "prof";
	public String amount = "1";	

	public String get_link()
	{
		return link;
	}

	public String get_email()
	{
		return email;
	}

	public String get_password()
	{
		return password;
	}
}