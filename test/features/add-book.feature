Feature: Adding a book

	Scenario: Admin add book with valid parameters
		Given User logged in system as Admin
		When Admin  goes to the Adding book page
		Then Admin enters a valid Title
		And Admin enter a valid Author
		And Admin enter a valid Tags
		And Admin  enter quantity of books in location
		And Admin request a book
