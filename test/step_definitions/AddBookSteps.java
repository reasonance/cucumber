package step_definitions;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.sendKeys;

import main.pages.LoginPage.SignInAction;
import main.pages.HomePage.HomePageAction;
import maim.pages.AddBookPage.AddBookAction;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;


public class AddBookStepDefs
{  
    public AddBookStepDefs()
    {
    	private SignInAction action_sI;
    	private HomePageAction action_hP;
    	private AddBookAction action_aB;
    }

    Given(/^User logged in system as Admin$/)
    public void SignIn()
    {
    	@FindBy(css = "#user_email")
    	private WebElement email;

    	action_sI.enter_email(email);


    	@FindBy(css = "#user_password")	
  		private WebElement password;

  		action_sI.enter_password(password);


    	@FindBy(css = ".btn btn-primary btn-lg btn-block")	
  		private WebElement signin;	  	
    	    	
    	action_sI.signIn(sigin);

    }

	When(/^Admin goes to the Adding book page$/) 
	public void SignIn()
    {
    	@FindBy(css = "html/body/nav/div[2]/div/a/strong")
  		private WebElement profile_button;
    	
    	action_hP.go_to_profile(profile_button);


    	@FindBy(css = "html/body/nav/div[2]/div/ul/li/a")
  		private WebElement add_book_button;
    	
    	action_hP.add_book(add_book_button);
    	
    }

	Then(/^Admin enters a valid Title$/)
	public void add_title()
	{
		@FindBy(css = "#book_title")
  		private WebElement title_area;

  		action_aB.enter_title(title_area);
  	}

	And(/^Admin enter a valid Author$/)
	public void add_author()
	{
	  	@FindBy(css = "#book_authors_name")
  		private WebElement author_area;

  		action_aB.enter_author(author_area);
  	}

	And(/^Admin enter a valid Tags$/)
	public void add_tags()
	{
		@FindBy(css = ".tag-editor ui-sortable")
		private WebElement tags_area;

		action_aB.enter_tags(tags_area);
  	}

	And(/^Admin enter quantity of books in location$/)
	public void add_amount_in_location()
	{
		@FindBy(css = "#book_location_1")
  		private WebElement location_area;

  		action_aB.enter_amount_in_location(location_area);
  	}

	And(/^Admin request a book$/)
	public void request_book()
	{
		@FindBy(css = "#book-create-new")
  		private WebElement create_button;

	  	@FindBy(".btn btn-primary stand-in-queue")
		private WebElement request_button;

		action_aB.request(create_button, request_button);
  	}

	
	//Then(/^Admin see added book in booklist$/) 
    
}